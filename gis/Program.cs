﻿using System;
using System.IO;
using System.Linq;

namespace gis
{
    class Program
    {
        private bool deepView = false;

        private bool areDirectoriesCount = true;

        private string extension = "";

        private string path = "";

        private string argFromArgs = "";

        static void Main(string[] args)
        {
            var App = new Program();
            Console.WriteLine($"RESULT: {App.Count(args)} with next values: dir = {App.path}, " +
                $"extension = '{App.extension}', deep = {App.deepView}");
            Console.ReadKey();
        }

        private int Count(string[] args)
        {
            foreach (var arg in args)
            {
                argFromArgs = arg.ToLower();
                if (argFromArgs.Contains("dir="))
                {
                    path = argFromArgs.Split('=')[1];
                    continue;
                }
                if (argFromArgs.Contains("deep="))
                {
                    deepView = argFromArgs.Split('=')[1].Equals("true") ? true : false;
                    continue;
                }
                if (argFromArgs.Contains("extension="))
                {
                    extension = argFromArgs.Split('=')[1];

                    if (extension[0] != '.') extension = $".{extension}";

                    deepView = true;
                    areDirectoriesCount = false;
                    continue;
                }
            }

            if (path.Trim().Equals("")) path = Environment.Is64BitOperatingSystem ?
                                                    @"C:\Program Files (x86)" :
                                                    @"C:\Program Files";

            if (!Directory.Exists(path))
            {
                Console.WriteLine("Directory not exists. Aborting");
                return -1;
            }

            if (!deepView) return Directory.GetDirectories(path).Length;

            return CountDirectoriesOrFiles(path);
        }

        private int CountDirectoriesOrFiles(string dirPath)
        {
            int result = 0;

            try
            {
                if (areDirectoriesCount) result += Directory.GetDirectories(dirPath).Length;
                else result += (from f in Directory.GetFiles(dirPath)
                                where f.ToLower().Substring(f.Length - extension.Length).Equals(extension.ToLower())
                                select f).Count();

                result += MoveDeeper(Directory.GetDirectories(dirPath));
            }
            catch (IOException exc)
            {
                Console.Error.WriteLine(exc.Message);
                return 0;
            }
            catch (UnauthorizedAccessException)
            {
                return 0;
            }
            return result;
        }

       
        private int MoveDeeper(string[] directories)
        {
            if (directories.Length == 0) return 0;

            int result = 0;

            foreach (string dir in directories)
            {
                result += CountDirectoriesOrFiles(dir);
            }

            return result;
        }


    }
}
